// Dependencies for dropdownComponent and listboxComponent:
// https://unpkg.com/@popperjs/core@2.4.4/dist/umd/popper.min.js
// https://unpkg.com/tippy.js@6.2.6/dist/tippy-bundle.umd.min.js


import {
  accordionComponent,
  alertComponent,
  collapsibleComponent,
  dialogComponent,
  dropdownComponent,
  listboxComponent,
  menubarComponent,
  menuNavigationComponent,
  tableAdvancedComponent,
  tabsComponent,
  tooltipComponent,
  toggleComponent,
  treeComponent,
  notificationComponent,
  radioButtonComponent,
  checkBoxComponent,
  MenuVerticalComponent,
  HeaderNavigationComponent,
  NavComponent
} from './desy-html.js';

var aria = aria || {};

accordionComponent(aria);
alertComponent(aria);
collapsibleComponent(aria);
dialogComponent(aria);
dropdownComponent(aria);
listboxComponent(aria);
menubarComponent(aria);
menuNavigationComponent(aria);
tableAdvancedComponent(aria);
tabsComponent(aria);
toggleComponent(aria);
tooltipComponent(aria);
treeComponent(aria);
notificationComponent(aria);
radioButtonComponent(aria);
checkBoxComponent(aria);
MenuVerticalComponent(aria);
HeaderNavigationComponent(aria);
NavComponent(aria);
